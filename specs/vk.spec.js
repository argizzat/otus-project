import { expect, test } from '@jest/globals';
import { apiProvider } from '../framework/index';
import { sleep } from '../src/sleep'

let profileInitialData;
let postId;

describe('Проектная работа \'Разработка фреймворка автоматизированного тестирования социальной сети\'', () => {
    test('1. Пользователь может получить информацию о своем профиле', async () => {
        const r = await apiProvider().vkAccountApi().getProfileInfo();
        console.log(r.body);
        expect(r.status).toEqual(200);
        expect(r.body.response).toBeDefined();
        profileInitialData = r.body.response;
        console.log(profileInitialData);
        expect(profileInitialData.id).toEqual(31901399);
        expect(profileInitialData.first_name).toEqual('Эрика');
        expect(profileInitialData.last_name).toEqual('Римберг');
    });

    test('2. Пользователь может редактировать информацию о себе', async () => {
        let sendData = {
            relation: 2,
            bdate: '26.12.1986',
        };
        const r = await apiProvider().vkAccountApi().postProfileInfo(sendData);
        console.log(r.body);
        expect(r.status).toEqual(200);
        expect(r.body.response).toBeDefined();
        expect(r.body.response.changed).toEqual(1);

        const updatedValues = await apiProvider().vkAccountApi().getProfileInfo();
        expect(updatedValues.body.response.relation).toEqual(sendData.relation);
        expect(updatedValues.body.response.bdate).toEqual(sendData.bdate);

        sleep(1000);
        sendData.relation = profileInitialData.relation;
        sendData.bdate = profileInitialData.bdate;
        const returnValues = await apiProvider().vkAccountApi().postProfileInfo(sendData);
        expect(returnValues.body.response).toBeDefined();
        expect(returnValues.body.response.changed).toEqual(1);
    });
    test('3. Пользователь может получать информацию о своем статусе и менять его', async () => {
        // получение статуса
        const r1 = await apiProvider().vkStatusApi().getStatus();
        console.log(r1.body);
        expect(r1.status).toEqual(200);
        expect(r1.body.response).toBeDefined();
        let initialStatus = r1.body.response.text;

        // установка нового статуса
        const sendData = {
            text: "OTUS THE BEST!!!"
        };
        const r2 = await apiProvider().vkStatusApi().setStatus(sendData);
        console.log(r2.body);
        expect(r2.status).toEqual(200);
        expect(r2.body.response).toBeDefined();
        expect(r2.body.response).toEqual(1);

        // проверка установленного статуса
        sleep(1000);
        const r3 = await apiProvider().vkStatusApi().getStatus();
        console.log(r3.body);
        expect(r3.body.response).toBeDefined();
        expect(r3.body.response.text).toEqual(sendData.text);

        // возврат изначального статуса
        sendData.text = initialStatus;
        const r4 = await apiProvider().vkStatusApi().setStatus(sendData);
        expect(r4.status).toEqual(200);
        expect(r4.body.response).toEqual(1);
    });
    test('4. Пользователь может получить список своих друзей', async () => {
        const r = await apiProvider().vkFriendsApi().getFriendsList();
        console.log(r.body);
        expect(r.status).toEqual(200);
        expect(r.body.response).toBeDefined();
        expect(r.body.response.count).toBeGreaterThanOrEqual(1);
    });
    test.each`
     queryString        | expectedId
     ${'Андрей Чирышев'}| ${8016569}
     ${'Виктор Брант'}  | ${182662565}
   `('5. Пользователь может осуществить поиск по своим друзьям по строке\'$queryString\'', async ({ queryString, expectedId }) => {
        const encodedQueryString = encodeURI(queryString);
        const r = await apiProvider().vkFriendsApi().searchFriends(encodedQueryString);
        console.log(r.body);
        expect(r.status).toEqual(200);
        expect(r.body.response).toBeDefined();
        expect(r.body.response.count).toBeGreaterThanOrEqual(1);
        let friendId = r.body.response.items[0].id;
        expect(friendId).toEqual(expectedId);
    });
    test('6. Пользователь может получить список записей со своей стены', async () => {
        const r = await apiProvider().vkWallApi().getPosts();
        console.log(r.body);
        expect(r.status).toEqual(200);
        expect(r.body.response).toBeDefined();
        expect(r.body.response.count).toBeGreaterThanOrEqual(1);

        postId = r.body.response.items.find(i => i.comments.count != 0).id;
    });
    test('7. Пользователь может получить список комментариев к записи на стене', async () => {
        const sendData = {
            post_id: postId
        };
        sleep(1000);
        const r = await apiProvider().vkWallApi().getComments(sendData);
        console.log(r.body);
        expect(r.status).toEqual(200);
        expect(r.body.response).toBeDefined();
        expect(r.body.response.count).toBeGreaterThanOrEqual(1);
    });
    test('9. Пользователь может создать запись на стене', async () => {
        const sendData = {
            message: 'I am trying to create this small post from my miniature test framework =)'
        };
        const r1 = await apiProvider().vkWallApi().createPost(sendData);
        console.log(r1.body);
        expect(r1.status).toEqual(200);
        expect(r1.body.response).toBeDefined();
        postId = r1.body.response.post_id;

        const r2 = await apiProvider().vkWallApi().getPosts();
        expect(r2.body.response.items.some(i => i.id == postId)).toBeTruthy();
        expect(r2.body.response.items.find(i => i.id === postId).text).toEqual(sendData.message);
    });
    test('8. Пользователь может добавить комментарий к записи на стене', async () => {
        sleep(1000);
        const sendData = {
            post_id: postId,
            message: 'Hello from Otus JS QA Engineer!'
        };
        const r1 = await apiProvider().vkWallApi().createComment(sendData);
        console.log(r1.body);
        expect(r1.status).toEqual(200);
        expect(r1.body.response).toBeDefined();
        const commentId = r1.body.response.comment_id;

        const r2 = await apiProvider().vkWallApi().getComments({ post_id: postId });
        expect(r2.body.response.items.some(i => i.id === commentId)).toBeTruthy();
    });
    test('10. Пользователь может удалить запись на стене', async () => {
        sleep(1000);
        const sendData = {
            post_id: postId
        };
        const r1 = await apiProvider().vkWallApi().deletePost(sendData);
        console.log(r1.body);
        expect(r1.status).toEqual(200);
        expect(r1.body.response).toBeDefined();
        expect(r1.body.response).toEqual(1);

        const r2 = await apiProvider().vkWallApi().getPosts();
        expect(r2.body.response.items.every(i => i.id != postId)).toBeTruthy();
    });
});
