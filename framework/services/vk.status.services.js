import supertest from 'supertest';
import { urls, profile } from '../config/index';

const VkStatusApi = function VkStatusApi() {
  this.getStatus = async function getStatus() {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/status.get?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json');
    return r;
  };
  this.setStatus = async function setStatus(sendData) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/status.set?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json')
      .query(sendData);
    return r;
  };  
};

export { VkStatusApi };
