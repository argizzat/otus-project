import supertest from 'supertest';
import { urls, group } from '../config/index';

const VkMessagesApi = function VkMessagesApi() {
  this.postCreateChat = async function postCreateChat(params) {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/messages.createChat?access_token=${group.accessToken}&v=${group.apiVersion}`)
      .set('Accept', 'application/json')
      .query(params);
    return r;
  };
  this.getChatInfo = async function getChatInfo(chatId) {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/messages.getChat?access_token=${group.accessToken}&v=${group.apiVersion}&chat_id=${chatId}`)
      .set('Accept', 'application/json');
    return r;
  };    
  this.getHistory = async function getHistory(chatId) {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/messages.getHistory?access_token=${group.accessToken}&v=${group.apiVersion}&peer_id=2000000000${chatId}`)
      .set('Accept', 'application/json');
    return r;
  };
  this.sendMessage = async function sendMessage(sendData) {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/messages.send?access_token=${group.accessToken}&v=${group.apiVersion}`)
      .set('Accept', 'application/json')
      .query(params);
    return r;
  };
  this.editMessage = async function editMessage(params) {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/messages.edit?access_token=${group.accessToken}&v=${group.apiVersion}`)
      .set('Accept', 'application/json')
      .query(params);
    return r;
  };
  this.deleteMessage = async function deleteMessage(params) {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/messages.delete?access_token=${group.accessToken}&v=${group.apiVersion}`)
      .set('Accept', 'application/json')
      .query(params);
    return r;
  };
};

export { VkMessagesApi };
