import supertest from 'supertest';
import { urls, profile } from '../config/index';

const VkFriendsApi = function VkFriendsApi() {
  this.getFriendsList = async function getFriendsList() {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/friends.get?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json');
    return r;
  };
  this.searchFriends = async function searchFriends(queryString) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/friends.search?access_token=${profile.accessToken}&v=${profile.apiVersion}&q=${queryString}`)
      .set('Accept', 'application/json');
    return r;
  };  
};

export { VkFriendsApi };
