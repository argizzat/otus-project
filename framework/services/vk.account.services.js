import supertest from 'supertest';
import { urls, profile } from '../config/index';

const VkAccountApi = function VkAccountApi() {
  this.getProfileInfo = async function getProfileInfo() {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/account.getProfileInfo?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json');
    return r;
  };
  this.postProfileInfo = async function postProfileInfo(sendData) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/account.saveProfileInfo?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json')
      .query(sendData);
    return r;
  };
  
};

export { VkAccountApi };
