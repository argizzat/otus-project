import supertest from 'supertest';
import { urls, profile } from '../config/index';

const VkWallApi = function VkWallApi() {
  this.getPosts = async function getPosts() {
    const r = await supertest(`${urls.vk}`)
      .get(`/method/wall.get?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json');
    return r;
  };
  this.getComments = async function getComments(sendData) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/wall.getComments?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json')
      .query(sendData);
    return r;
  };  
  this.createComment = async function createComment(sendData) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/wall.createComment?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json')
      .query(sendData);
    return r;
  };
  this.createPost = async function createPost(sendData) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/wall.post?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json')
      .query(sendData);
    return r;
  };  
  this.deletePost = async function deletePost(sendData) {
    const r = await supertest(`${urls.vk}`)
      .post(`/method/wall.delete?access_token=${profile.accessToken}&v=${profile.apiVersion}`)
      .set('Accept', 'application/json')
      .query(sendData);
    return r;
  };  
};

export { VkWallApi };
