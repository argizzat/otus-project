import { VkAccountApi, VkStatusApi, VkFriendsApi, VkMessagesApi, VkWallApi }  from './services/index';

const apiProvider = () => ({
  vkAccountApi: () => new VkAccountApi(),
  vkStatusApi: () => new VkStatusApi(),
  vkFriendsApi: () => new VkFriendsApi(),
  vkMessagesApi: () => new VkMessagesApi(),
  vkWallApi: () => new VkWallApi()
});

export { apiProvider };
